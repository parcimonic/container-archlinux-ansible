# hadolint ignore=DL3007
FROM archlinux:latest

# Add sudo so we can `become_user` in Ansible.
RUN set -euf && \
    pacman -Syu --noconfirm --noprogressbar \
    python \
    sudo

# Add a non-root user for some Ansible tasks that
# target unprivileged users.
RUN useradd -m milkyway
