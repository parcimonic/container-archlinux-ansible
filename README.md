# Arch Linux container

This is an [Arch](https://archlinux.org/) based image that I'm using for my
[Molecule](https://molecule.readthedocs.io/en/latest/index.html) tests.

URL: `registry.gitlab.com/parcimonic/container-archlinux-ansible:latest`
